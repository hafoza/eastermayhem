# EasterMayhem
To run the build, just enter the "Build" folder and run the executable.

If you would like to check the source code, you can find it on the EasterMayhem_Unity/Assets/AISpells/Scripts, EasterMayhem_Unity/Assets/Spells/Scripts and EasterMayhem_Unity/Assets/Resources/Scripts paths. Some scripts which you may find interesting are the ones related to Spells(AISpells and Spells, followed by "Scripts" in their path), Items (Resources/Scripts) and playerStats(Resources/Scripts), but not only.