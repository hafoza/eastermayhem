﻿using UnityEngine;
using System.Collections;

public class objectiveComplete : MonoBehaviour {

    public enemyStats eStats;

    void Start()
    {
        eStats = GetComponent<enemyStats>();
    }

	void Update () {
        if (eStats.dead)
        {
            if (eStats.spawnedBy.name == "BlindBunny")
            {
                objectiveTracker.instance.objective1 = true;    
            }
            if (eStats.spawnedBy.name == "HellishBunny")
            {
                objectiveTracker.instance.objective2 = true;
            }
            if (eStats.spawnedBy.name == "BugsBunny")
            {
                Invoke("delay", 0.5F);
            }
        }
	}

    void delay()
    {
        objectiveTracker.instance.objective3 = true;
        this.enabled = false;
    }
}
