﻿using UnityEngine;
using System.Collections;

public class EnemyAI : MonoBehaviour {

    private NavMeshAgent agent;
    private Animator animator;
    [HideInInspector]
    public GameObject Hero;
    private float speedFactor, targetSpeedFactor;
    private enemyStats enemy;
    public bool foundPlayer;
    public Vector3 dest;

	void Start () 
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        agent.stoppingDistance = 0.2f;
        enemy = GetComponent<enemyStats>();
	}


    void Update()
    {
        if (Vector3.Distance(transform.position, Hero.transform.position) <= enemy.range && !enemy.dead && enemy.range > 0f)
            foundPlayer = true;

        if (enemy.dead || Hero.GetComponent<playerStats>().dead)
            foundPlayer = false;

        if (foundPlayer)
        {
            dest = (Hero.transform.position);
            agent.SetDestination(dest);
            if (agent.hasPath)
            {
                targetSpeedFactor = Mathf.Clamp01(agent.velocity.magnitude);
                speedFactor = Mathf.Lerp(speedFactor, targetSpeedFactor, Time.deltaTime * 4f);

            }
            else
            {
                targetSpeedFactor = 0;
                speedFactor = Mathf.Lerp(speedFactor, targetSpeedFactor, Time.deltaTime * 10f);
            }

            if (agent.remainingDistance < 1f && agent.hasPath)
            {
                speedFactor = 0f;
            }

            Quaternion lookRotation = Quaternion.LookRotation(((Hero.transform.position - new Vector3(0, Hero.transform.position.y, 0))
                - (transform.position - new Vector3(0, transform.position.y, 0))).normalized);
            transform.rotation = lookRotation;

            animator.SetFloat("Speed", speedFactor);
        }

    }

    void OnAnimatorMove()
    {
        float animation_position;

        animation_position = (animator.deltaPosition.magnitude > 0.1f) ? animator.deltaPosition.magnitude : 0.1f;

        if (agent.remainingDistance > 2f && Time.timeScale != 0)
            agent.velocity = Vector3.Lerp(agent.velocity, agent.desiredVelocity.normalized * enemy.movementSpeed * (animation_position / Time.deltaTime), Time.deltaTime * 4f);
        else
            agent.velocity = Vector3.zero;



        if (agent.desiredVelocity != Vector3.zero)
        {
            Quaternion lookRotation = Quaternion.LookRotation(agent.desiredVelocity);
            transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, agent.angularSpeed * Time.deltaTime);
        }
    }

    void OnDrawGizmos()
    {
        if(foundPlayer)
            Gizmos.DrawWireSphere(dest, 1);
    
    }
}
