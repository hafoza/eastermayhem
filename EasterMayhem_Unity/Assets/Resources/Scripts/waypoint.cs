﻿using UnityEngine;
using System.Collections;

public class waypoint : MonoBehaviour {

    public float radius;
    public GameObject player;

	void Update () {

        if (Vector3.Distance(transform.position, player.transform.position) < radius)
            player.GetComponent<playerStats>().respawnPosition = transform.position + new Vector3(1, -transform.position.y + 0.1F, 1);
	}
}
