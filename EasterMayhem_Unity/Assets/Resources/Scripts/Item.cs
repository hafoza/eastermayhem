﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Item {

    public string name;
    public int healthRegen;
    public int manaRegen;
    public int constitution;
    public int strength;
    public int wisdom;
    public int armor;
    public float movementSpeed;
    public enum RarityType { Common, Rare, Legendary, Ancient};
    public RarityType rarity;
    public enum ItemType { NoType, Helmet, Chest, Pants, Gloves, Boots, Sword };
    public ItemType type;
    public string flavorText;
    public int backpackIndex;

    public Item(int iHealthRegen, int iManaRegen, int iConstitution, int iStrength, int iWisdom, 
        int iArmor, float iMs, RarityType iRarity, ItemType iType, string iName, string iFlavorText)
    {

        name = iName;
        healthRegen = iHealthRegen;
        manaRegen = iManaRegen;
        constitution = iConstitution;
        strength = iStrength;
        wisdom = iWisdom;
        armor = iArmor;
        movementSpeed = iMs;
        rarity = iRarity;
        type = iType;
        flavorText = iFlavorText;
        backpackIndex = -1;
    }
}
