﻿using UnityEngine;
using System.Collections;

public class collisionDetection : MonoBehaviour {

    public Animator animator;
    public Collider thisCollider,tempOtherCollider;
    public bool collided = false;
    
    public string tst;

	void Awake () {

        thisCollider = GetComponent<Collider>();

	}

    void Start()
    {
        animator = GetComponentInParent<Animator>();
    }

    void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(1).fullPathHash == Animator.StringToHash("Attack.Idle"))
            collided = false;

    }
	
    void OnTriggerEnter(Collider otherCollider)
    {

        tempOtherCollider = otherCollider;

        if (otherCollider.tag == "HitBox" && !collided && thisCollider.GetComponentInParent<playerStats>() && 
            otherCollider.GetComponentInParent<enemyStats>() && thisCollider.GetComponentInParent<BasicAttack>().hit &&
        thisCollider.GetComponentInParent<playerStats>().gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).fullPathHash == Animator.StringToHash("Attack.Punch"))
        {
            playerStats pStats = thisCollider.GetComponentInParent<playerStats>();
            enemyStats eStats = otherCollider.GetComponentInParent<enemyStats>();


            eStats.modifyHP((int)(pStats.strength * 2.5F), -1);

            if (eStats.currentHP <= 0)
            {
                otherCollider.enabled = false;
                eStats.die(pStats.gameObject);
                
            }
            collided = true;

        }
        if ( otherCollider.tag == "HitBox" && !collided && thisCollider.GetComponentInParent<enemyStats>() && 
            otherCollider.GetComponentInParent<playerStats>() && thisCollider.GetComponentInParent<Attack>().hit &&
            thisCollider.GetComponentInParent<enemyStats>().gameObject.GetComponent<Animator>().GetCurrentAnimatorStateInfo(1).fullPathHash == Animator.StringToHash("Attack.Punch"))
        {
            playerStats pStats = otherCollider.GetComponentInParent<playerStats>();
            enemyStats eStats = thisCollider.GetComponentInParent<enemyStats>();
            pStats.modifyHP(eStats.damage, -1);

            /*if (pStats.currentHP <= 0)
            {
                pStats.currentHP = 0;
                pStats.die();
            }*/
            collided = true;

        }
    }

    void reEnableCollider()
    {
        tempOtherCollider.enabled = true;
    }


}
