﻿using UnityEngine;
using System.Collections;

public class Loot : MonoBehaviour {

    public GameObject player;
    public ItemGenerator itemFactory = new ItemGenerator();
    public Item[] items = new Item[3];
    public int numberOfItems;
    private int i, rand;
    public mouseInput input;
    public UIControl ui;

    public string[] names = { "Cozonac", "Trap card", "Swag ", "Ladies Magnet", "Surgeon Gloves", "Leopard - Print Pants",
                              "Yoga Pants", "Baggy Pants", "Blue and Black Shirt", "Thick Vest", "Tank Top",
                            "Propeller hat", "Thug Bandana", "Crown of thorns", "The Cross", "Brass - Knuckles",
                            "Dry Cooker", "Eyepatch", "KebABS", "Nike’s", "Skinny Jeans", "Leather Gloves",
                            "Middle finger mittens", "Oven gloves", "Lance of Longinus", "Sandals", 
                            "Jesus’s Magic Sandals", "Winged shoes"};

    public string[] flavorText = { "Just can’t get enough.", "You just activated it.", "YOLO’s almost forgotten brother.", 
                                     "Ladies love magnets.", "Everyone can be a surgeon.", "For the cougar inside you.", 
                                     "Booty aplenty.", "This close to falling off.", "It’s white and gold actually.", 
                                     "Winter for your chest. Spring for your arms.", "Molds on those wonderful abs.", 
                                     "For men with wasted childhoods.", "The thug life didn’t choose you, surprisingly.", 
                                     "If only it would protect as much as it hurts.", "Everything can be used for fighting if you are brave enough.", 
                                     "Up close and personal. ", "Now you can cook your enemies to death.", 
                                     "It looks like this has been dropped by a bunny.", "Now you can have kebab and ABS at the same time. ",
                                     "Just do it.", "Tight around the crotch.", 
                                     "For grabbing things.", "To use whenever someone upsets you.", "Straight outta hell’s kitchen.", 
                                     "Isn’t it weird to fight using something that pierced you? … Ouch, my shoulder!", 
                                     "The swords have been lost.", "How do you think he managed to walk on water. It was the shoes all along. ", 
                                     "Don’t actually let you fly."};

    public Item.ItemType[] types = { Item.ItemType.NoType, Item.ItemType.NoType, Item.ItemType.NoType, Item.ItemType.NoType,
                                   Item.ItemType.Gloves, Item.ItemType.Pants, Item.ItemType.Pants, Item.ItemType.Pants, Item.ItemType.Chest,
                                   Item.ItemType.Chest, Item.ItemType.Chest, Item.ItemType.Helmet, Item.ItemType.Helmet,
                                   Item.ItemType.Helmet, Item.ItemType.Sword, Item.ItemType.Sword, Item.ItemType.Sword,
                                   Item.ItemType.Helmet, Item.ItemType.Chest, Item.ItemType.Boots, Item.ItemType.Pants,
                                   Item.ItemType.Gloves, Item.ItemType.Gloves, Item.ItemType.Gloves, Item.ItemType.Sword,
                                   Item.ItemType.Boots, Item.ItemType.Boots, Item.ItemType.Boots};

	void Start () 
    {
        player = GameObject.FindGameObjectWithTag("Jesus");
        input = Camera.main.GetComponent<mouseInput>();
        ui = GameObject.FindGameObjectWithTag("UI").GetComponent<UIControl>();

        Invoke("selfDestruct", 20);

        for (i = 0; i < 3; i++)
            items[i] = null;

            for (i = 0; i < numberOfItems; i++)
            {
                rand = (int)Random.Range(0, names.Length);
                items[i] = itemFactory.GenerateItem(names[rand], flavorText[rand], types[rand]);
            }

	}
	
	void Update () 
    {

        if (input.showLoot && player.GetComponent<playerStats>().selectedLoot == gameObject)
        {
            ui.lootWindow.alpha = 1;
            ui.lootWindow.interactable = true;
            ui.lootWindow.blocksRaycasts = true;

            for (i = 0; i < numberOfItems; i++)
            {
                ui.itemName[i].enabled = true;
                ui.itemIcon[i].enabled = true; ;

                if (items[i].rarity == Item.RarityType.Common)
                    ui.itemName[i].color = ui.commonColor;

                if (items[i].rarity == Item.RarityType.Rare)
                    ui.itemName[i].color = ui.rareColor;

                if (items[i].rarity == Item.RarityType.Legendary)
                    ui.itemName[i].color = ui.legendaryColor;

                if (items[i].rarity == Item.RarityType.Ancient)
                    ui.itemName[i].color = ui.ancientColor;

                ui.itemName[i].text = items[i].name;

                if (items[i].type == Item.ItemType.Helmet)
                {
                    ui.itemIcon[i].sprite = ui.iconSources[0];
                }

                if (items[i].type == Item.ItemType.Chest)
                {
                    ui.itemIcon[i].sprite = ui.iconSources[1];
                }

                if (items[i].type == Item.ItemType.Gloves)
                {
                    ui.itemIcon[i].sprite = ui.iconSources[2];
                }

                if (items[i].type == Item.ItemType.Sword)
                {
                    ui.itemIcon[i].sprite = ui.iconSources[3];
                }

                if (items[i].type == Item.ItemType.Pants)
                {
                    ui.itemIcon[i].sprite = ui.iconSources[4];
                }

                if (items[i].type == Item.ItemType.Boots)
                {
                    ui.itemIcon[i].sprite = ui.iconSources[5];
                }
            }

            for (i = 0; i < 3; i++)
                if (items[i] == null)
                {
                    ui.itemName[i].enabled = false;
                    ui.itemIcon[i].enabled = false;
                }
        }

        if (numberOfItems == 0)
        {
            input.showLoot = false;
            ui.lootWindow.alpha = 0;
            ui.lootWindow.interactable = false;
            ui.lootWindow.blocksRaycasts = false;
            GameObject.Destroy(gameObject);
        }
            
	}

    public void PickUpItem(int i)
    {
        
        if (player.GetComponent<Inventory>().AddItem(items[i]))
        {
            if (i < (numberOfItems-1))
            {
                for (int j = i; j < numberOfItems-1; j++)
                    items[j] = items[j + 1];
            }

            //player.GetComponent<Inventory>().AddItem(items[i]);

            items[(numberOfItems - 1)] = null;
            
            numberOfItems--;     
        }
          
    }
    public void PickUpEverything() 
    {
        for (int i = 0; i < numberOfItems; i++)
        {
            if (player.GetComponent<Inventory>().AddItem(items[i]))
            {
                for (int j = i; j < numberOfItems-1; j++)
                    items[j] = items[j + 1];

                items[numberOfItems].type = Item.ItemType.NoType;
                numberOfItems--;
            }
            
        }
    }

    void selfDestruct()
    {
        if (input.showLoot && player.GetComponent<playerStats>().selectedLoot == gameObject)
        {
            input.showLoot = false;
            ui.lootWindow.alpha = 0;
            ui.lootWindow.interactable = false;
            ui.lootWindow.blocksRaycasts = false;
        }
        GameObject.Destroy(gameObject);
    }

}
