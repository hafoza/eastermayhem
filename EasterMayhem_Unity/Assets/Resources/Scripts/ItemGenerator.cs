﻿using UnityEngine;
using System.Collections;

public class ItemGenerator {

    Item.RarityType iRarity;
    Item.ItemType iType;

	public Item GenerateItem(string ItemName, string FlavorText, Item.ItemType type = Item.ItemType.NoType)
    {
        int iHealthRegen, iManaRegen, iConstitution, iStrength, iWisdom, iArmor;
        float iMs;
        float random = Random.value;
        //int modifier = 1;

        if(random <= 0.50f)
        {
            iRarity = Item.RarityType.Common; 
            //modifier = 1;
        }
        else
            if(random <= 0.75f)
            {
                iRarity = Item.RarityType.Rare;
                //modifier = 2;
            }
        else
            if(random <= 0.90f)
            { 
                iRarity = Item.RarityType.Legendary;
                //modifier = 3;
            }
        else
            if (random <= 1.0f)
            { 
                iRarity = Item.RarityType.Ancient;
                //modifier = 5;
            }

        if (type == Item.ItemType.NoType)
            iType = (Item.ItemType)Random.Range((int)Item.ItemType.Helmet, (int)Item.ItemType.Sword + 1);
        else
            iType = type;

        string iName = ItemName;

        if(iRarity == Item.RarityType.Common)
            iHealthRegen = (int)(Random.Range(0f, 1f));
        else if(iRarity == Item.RarityType.Rare)
            iHealthRegen = (int)(Random.Range(0f, 2f));
        else if(iRarity == Item.RarityType.Legendary)
            iHealthRegen = (int)(Random.Range(1f, 3f));
        else //if(iRarity == Item.RarityType.Ancient)
            iHealthRegen = (int)(Random.Range(2f, 4f));

        if(iRarity == Item.RarityType.Common)
            iManaRegen = (int)(Random.Range(0f, 1f));
        else if(iRarity == Item.RarityType.Rare)
            iManaRegen = (int)(Random.Range(0f, 2f));
        else if(iRarity == Item.RarityType.Legendary)
            iManaRegen = (int)(Random.Range(1f, 3f));
        else //if(iRarity == Item.RarityType.Ancient)
            iManaRegen = (int)(Random.Range(2f, 4f));
        
        if(iType == Item.ItemType.Chest || iType == Item.ItemType.Pants)
        {
        if(iRarity == Item.RarityType.Common)
            iConstitution = (int)(Random.Range(1f, 5f));
        else if(iRarity == Item.RarityType.Rare)
            iConstitution = (int)(Random.Range(5f, 10f));
        else if(iRarity == Item.RarityType.Legendary)
            iConstitution = (int)(Random.Range(10f, 15f));
        else //if(iRarity == Item.RarityType.Ancient)
            iConstitution = (int)(Random.Range(20f, 30f));
        }
        else
            iConstitution = 0;

        if(iType == Item.ItemType.Gloves || iType == Item.ItemType.Sword)
        {
        if(iRarity == Item.RarityType.Common)
            iStrength = (int)(Random.Range(1f, 5f));
        else if(iRarity == Item.RarityType.Rare)
            iStrength = (int)(Random.Range(5f, 10f));
        else if(iRarity == Item.RarityType.Legendary)
            iStrength = (int)(Random.Range(10f, 15f));
        else //if(iRarity == Item.RarityType.Ancient)
            iStrength = (int)(Random.Range(20f, 30f));
        }
        else
            iStrength = 0;

        if(iType == Item.ItemType.Helmet)
        {
        if(iRarity == Item.RarityType.Common)
            iWisdom = (int)(Random.Range(1f, 5f));
        else if(iRarity == Item.RarityType.Rare)
            iWisdom = (int)(Random.Range(5f, 10f));
        else if(iRarity == Item.RarityType.Legendary)
            iWisdom = (int)(Random.Range(10f, 15f));
        else //if(iRarity == Item.RarityType.Ancient)
            iWisdom = (int)(Random.Range(20f, 30f));
        }
        else
            iWisdom = 0;

        if (iType != Item.ItemType.Sword)
        {
        if(iRarity == Item.RarityType.Common)
            iArmor = (int)(Random.Range(1f, 10f));
        else if(iRarity == Item.RarityType.Rare)
            iArmor = (int)(Random.Range(10f, 50f));
        else if(iRarity == Item.RarityType.Legendary)
            iArmor = (int)(Random.Range(50f, 100f));
        else //if(iRarity == Item.RarityType.Ancient)
            iArmor = (int)(Random.Range(100f, 200f));
        }
        else
            iArmor = 0;

        if(iType == Item.ItemType.Boots)
        {
        if(iRarity == Item.RarityType.Common)
            iMs = Random.Range(0f, 0.1f);
        else if(iRarity == Item.RarityType.Rare)
            iMs = Random.Range(0.1f, 0.2f);
        else if(iRarity == Item.RarityType.Legendary)
            iMs = Random.Range(0.2f, 0.3f);
        else //if(iRarity == Item.RarityType.Ancient)
            iMs = Random.Range(0.3f, 0.5f);
        }
        else
            iMs = 0;

        string iFlavorText = FlavorText;

        Item item = new Item(iHealthRegen, iManaRegen, iConstitution, iStrength, iWisdom, iArmor, iMs, iRarity, iType, iName, iFlavorText);
        return item;
    }


}
