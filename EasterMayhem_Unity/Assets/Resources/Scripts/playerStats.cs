﻿using UnityEngine;
using System.Collections;


[System.Serializable]
public class playerStats : MonoBehaviour
{
    [Range(1.0f, 2.0f)]
    public float movementSpeed;
    public Vector3 respawnPosition;
    public Animator animator;
    public int maxHP, currentHP, regenHP;
    public int maxMP, currentMP, regenMP;
    public int constitution, strength, wisdom;
    public int armor;
    public int level, currentExp, requiredExp, lastLevelExp;
    public GameObject target;
    public bool dead = false;
    public GameObject damageTextSource,selectedLoot;

    void Start()
    {
        animator = GetComponent<Animator>();
        InvokeRepeating("HealthRegen", 1, 1);
        InvokeRepeating("ManaRegen", 1, 1);
    }

    //Default constructor with magic numbahs
    public playerStats()
    {
        movementSpeed = 1.0F;

        constitution = 10;
        strength = 10;
        wisdom = 10;

        level = 1;
        currentExp = 0;
        requiredExp = 100;
        lastLevelExp = 0;

        maxHP = currentHP = constitution * 10;
        maxMP = currentMP = wisdom * 5;

        regenHP = 1;
        regenMP = 1;

        armor = 0;
    }

    //Function for damage/healing
    public void modifyHP(int modifier, int type)
    {
        if (type == -1)
        {
            currentHP -= (int)(((modifier * (100 - (int)(Mathf.Log(armor) * 10))) / 100));
            if (currentHP < 0)
            {
                currentHP = 0;
                die();
            }
        }

        if (type == 1)
        {
            currentHP += modifier;
        }


        GameObject damageText = (GameObject)GameObject.Instantiate(damageTextSource, transform.position + (Vector3.up * 5), Quaternion.identity);
        damageText.GetComponent<floatingDamageText>().type = 0;
        damageText.GetComponent<floatingDamageText>().damage = ((int)((modifier * (100 - (int)(Mathf.Log(armor) * 10))) / 100)).ToString();

    
    }

    //Function for mana loss/gain
    public void modifyMP(int modifier, int type)
    {
        if (type == -1)
        {
            currentMP -= modifier;
        }

        if (type == 1)
        {
            currentMP += modifier;
        }

    }

    private void HealthRegen()
    {
        currentHP += regenHP;
        if (currentHP > maxHP)
            currentHP = maxHP;
    }

    private void ManaRegen()
    {
        currentMP += regenMP;
        if (currentMP > maxMP)
            currentMP = maxMP;
    }

    //Quick Level Up function for funsies
    public void levelUp()
    {
        level++;
        lastLevelExp = requiredExp;
        requiredExp = (int)(requiredExp * 1.5F);
        constitution = (int)(constitution + (5 * level));

        strength = (int)(strength + level);
        wisdom = (int)(wisdom + level);

        currentHP = maxHP = constitution * 10;
        currentMP = maxMP = wisdom * 5;
    }

    public void increaseExperience(int exp)
    {
        currentExp += exp;
        while (currentExp >= requiredExp)
        {
            currentExp = currentExp - requiredExp;
            levelUp();
            
        }
    
    }

    
    public void die()
    {
        if (!dead)
        {
            if (currentExp - (int)(0.1F * requiredExp) > 0)
                currentExp -= (int)(0.1F * requiredExp);
            else
                currentExp = 0;

            animator.SetTrigger("Dead");
            dead = true;
        }
    }

    public void respawn()
    {
        currentHP = maxHP;
        currentMP = maxMP;
        transform.position = respawnPosition;
        GetComponent<NavMeshAgent>().Warp(respawnPosition);
        dead = false;
    }


    void Update()
    {
        if(selectedLoot)
        { 
            if (Vector3.Distance(transform.position, selectedLoot.transform.position) > 3.5F)
            {
                selectedLoot = null;
                Camera.main.GetComponent<mouseInput>().showLoot = false;
            }
        }
        if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == Animator.StringToHash("Movement.Dieded") && dead)
        {
            respawn();
            animator.SetTrigger("Respawn");
            dead = false;
            Camera.main.GetComponent<mouseInput>().inputWorldPosition = transform.position;
        }
    }
}

