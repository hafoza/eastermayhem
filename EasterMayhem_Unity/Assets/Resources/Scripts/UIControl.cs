﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UIControl : MonoBehaviour
{

    public Image mana, health, exp;
    public playerStats pStats;
    public GameObject Hero;
    public Text targetName, targetLevel, healthText, manaText, expText, levelText, targetHealth, equippedRarity, backpackRarity;
    public CanvasGroup targetCG;
    public Inventory inv;
    public CanvasGroup[] skillTooltips;
    public int[] cds;

    public bool[] showTooltip;

    public Sprite[] iconSources;
    public Color commonColor, rareColor, legendaryColor, ancientColor;

    public Text[] backPack, cooldownsText, keybindings, manaCosts;
    public Image[] equipped, cooldownImages;
    public Image[] backPackIcons;
    public CanvasGroup inventoryCG, leftTab, rightTab, lootWindow;
    public Text[] characterProfile;

    public Text backpackSelectedName, backpackSelectedStats, backpackSelectedFlavorText;
    public Text equippedSelectedName, equippedSelectedStats, equippedSelectedFlavorText;
    public Text[] itemName;
    public Image[] itemIcon, hoverImages;

    public Canvas canvas, settings;

    public int selectedBackpackSlot;
    public string selectedEquippedSlot;


    void Start()
    {

        cds = new int[6];
        showTooltip = new bool[6];

        InvokeRepeating("reduceCooldown", 0, 1);

        canvas = GetComponent<Canvas>();

        Hero = GameObject.FindGameObjectWithTag("Jesus");
        inv = Hero.GetComponent<Inventory>();
        selectedBackpackSlot = -1;
        selectedEquippedSlot = "None";
    }

    public void pickUpItem(int i)
    {
        Hero.GetComponent<playerStats>().selectedLoot.GetComponent<Loot>().PickUpItem(i);
    }
    public void pickUpEverything()
    {
        Hero.GetComponent<playerStats>().selectedLoot.GetComponent<Loot>().PickUpEverything();
    }

    public void selectedBackpack(int i)
    {
        if (inv.backpack[i] != null)
        {
            selectedBackpackSlot = i;
            backpackSelectedStats.text = "";

            Item currentItem = inv.backpack[i];
            backpackSelectedName.text = currentItem.name;

            if (currentItem.rarity == Item.RarityType.Common)
            {
                backpackSelectedName.color = commonColor;
                backpackRarity.text = "Common";
                backpackRarity.color = commonColor;
            }
            if (currentItem.rarity == Item.RarityType.Rare)
            {
                backpackSelectedName.color = rareColor;
                backpackRarity.text = "Rare";
                backpackRarity.color = rareColor;
            }
            if (currentItem.rarity == Item.RarityType.Legendary)
            {
                backpackSelectedName.color = legendaryColor;
                backpackRarity.text = "Legendary";
                backpackRarity.color = legendaryColor;
            }
            if (currentItem.rarity == Item.RarityType.Ancient)
            {
                backpackSelectedName.color = ancientColor;
                backpackRarity.text = "Ancient";
                backpackRarity.color = ancientColor;
            }

            if (currentItem.armor != 0)
            {
                backpackSelectedStats.text += "Armor: " + currentItem.armor.ToString() + "\n";
            }
            if (currentItem.manaRegen != 0)
            {
                backpackSelectedStats.text += "Mana regen: " + currentItem.manaRegen.ToString() + "/s" + "\n";
            }
            if (currentItem.healthRegen != 0)
            {
                backpackSelectedStats.text += "Health regen: " + currentItem.healthRegen.ToString() + "/s" + "\n";
            }
            if (currentItem.constitution != 0)
            {
                backpackSelectedStats.text += "Constitution: " + currentItem.constitution.ToString() + "\n";
            }
            if (currentItem.strength != 0)
            {
                backpackSelectedStats.text += "Strength: " + currentItem.strength.ToString() + "\n";
            }
            if (currentItem.wisdom != 0)
            {
                backpackSelectedStats.text += "Wisdom: " + currentItem.wisdom.ToString() + "\n";
            }
            if (currentItem.movementSpeed != 0)
            {
                backpackSelectedStats.text += "Movement Speed: " + ((int)(currentItem.movementSpeed * 100)).ToString() + " %" + "\n";
            }

            backpackSelectedFlavorText.text = '"' + currentItem.flavorText + '"';
        }
        else
        {
            selectedBackpackSlot = -1;
            backpackSelectedName.text = "";
            backpackSelectedStats.text = "";
            backpackSelectedFlavorText.text = "";
        }


    }

    public void selectedEquipped(string slot)
    {
        if (inv.character[slot] != null)
        {
            selectedEquippedSlot = slot;
            equippedSelectedStats.text = "";

            Item currentItem = inv.character[slot];
            equippedSelectedName.text = currentItem.name;

            if (currentItem.rarity == Item.RarityType.Common)
            {
                equippedSelectedName.color = commonColor;
                equippedRarity.text = "Common";
                equippedRarity.color = commonColor;
            }
            if (currentItem.rarity == Item.RarityType.Rare)
            {
                equippedSelectedName.color = rareColor;
                equippedRarity.text = "Rare";
                equippedRarity.color = rareColor;
            }
            if (currentItem.rarity == Item.RarityType.Legendary)
            {
                equippedSelectedName.color = legendaryColor;
                equippedRarity.text = "Legendary";
                equippedRarity.color = legendaryColor;
            }
            if (currentItem.rarity == Item.RarityType.Ancient)
            {
                equippedSelectedName.color = ancientColor;
                equippedRarity.text = "Ancient";
                equippedRarity.color = ancientColor;
            }
            if (currentItem.armor != 0)
            {
                equippedSelectedStats.text += "Armor: " + currentItem.armor.ToString() + "\n";
            }
            if (currentItem.manaRegen != 0)
            {
                equippedSelectedStats.text += "Mana regen: " + currentItem.manaRegen.ToString() + "/s" + "\n";
            }
            if (currentItem.healthRegen != 0)
            {
                equippedSelectedStats.text += "Health regen: " + currentItem.healthRegen.ToString() + "/s" + "\n";
            }
            if (currentItem.constitution != 0)
            {
                equippedSelectedStats.text += "Constitution: " + currentItem.constitution.ToString() + "\n";
            }
            if (currentItem.strength != 0)
            {
                equippedSelectedStats.text += "Strength: " + currentItem.strength.ToString() + "\n";
            }
            if (currentItem.wisdom != 0)
            {
                equippedSelectedStats.text += "Wisdom: " + currentItem.wisdom.ToString() + "\n";
            }
            if (currentItem.movementSpeed != 0)
            {
                equippedSelectedStats.text += "Movement Speed: " + ((int)(currentItem.movementSpeed * 100)).ToString() + " %" + "\n";
            }

            equippedSelectedFlavorText.text = '"' + currentItem.flavorText + '"';
        }
        else
        {
            selectedEquippedSlot = "None";
            equippedSelectedName.text = "";
            equippedSelectedStats.text = "";
            equippedSelectedFlavorText.text = "";
        }
    }

    public void destroyBackPack()
    {
        if (selectedBackpackSlot != -1)
        {
            inv.DestroyItem(ref inv.backpack[selectedBackpackSlot]);
            backpackSelectedName.text = "";
            backpackSelectedStats.text = "";
            backpackSelectedFlavorText.text = "";

            selectedBackpackSlot = -1;
        }
    }

    public void destroyEquipped()
    {
        if (selectedEquippedSlot != "None")
        {
            inv.DestroyItemEquipped(inv.character[selectedEquippedSlot]);
            equippedSelectedName.text = "";
            equippedSelectedStats.text = "";
            equippedSelectedFlavorText.text = "";

            selectedEquippedSlot = "None";
        }
    }

    public void equip()
    {
        if (selectedBackpackSlot != -1)
        {
            inv.Equip(ref inv.backpack[selectedBackpackSlot]);
            backpackSelectedName.text = "";
            backpackSelectedStats.text = "";
            backpackSelectedFlavorText.text = "";
            selectedBackpackSlot = -1;
            selectedEquippedSlot = "None";
        }
    }

    public void unequip()
    {
        if (selectedEquippedSlot != "None")
        {

            equippedSelectedName.text = "";
            equippedSelectedStats.text = "";
            equippedSelectedFlavorText.text = "";

            if (selectedEquippedSlot == "Helmet")
            {
                inv.Unequip(Item.ItemType.Helmet);
            }
            if (selectedEquippedSlot == "Chest")
            {
                inv.Unequip(Item.ItemType.Chest);
            }
            if (selectedEquippedSlot == "Gloves")
            {
                inv.Unequip(Item.ItemType.Gloves);
            }
            if (selectedEquippedSlot == "Sword")
            {
                inv.Unequip(Item.ItemType.Sword);
            }
            if (selectedEquippedSlot == "Pants")
            {
                inv.Unequip(Item.ItemType.Pants);
            }
            if (selectedEquippedSlot == "Boots")
            {
                inv.Unequip(Item.ItemType.Boots);
            }

            selectedEquippedSlot = "None";
        }
    }

    void Update()
    {
        if (canvas.enabled)
        {
            characterProfile[0].text = pStats.armor.ToString();
            characterProfile[1].text = pStats.strength.ToString();
            characterProfile[2].text = pStats.constitution.ToString();
            characterProfile[3].text = pStats.wisdom.ToString();
            if (((int)(pStats.movementSpeed * 100)) >= 100)
                characterProfile[4].text = ((int)(pStats.movementSpeed * 100)).ToString() + "%";
            else
                characterProfile[4].text = "100%";
            characterProfile[5].text = pStats.regenHP.ToString() + " /s";
            characterProfile[6].text = pStats.regenMP.ToString() + " /s";

            if (selectedEquippedSlot == "None")
            {
                leftTab.alpha = 0;
                leftTab.interactable = false;
                leftTab.blocksRaycasts = false;

            }
            else
            {
                leftTab.alpha = 1;
                leftTab.interactable = true;
                leftTab.blocksRaycasts = true;
            }

            if (selectedBackpackSlot == -1)
            {
                rightTab.alpha = 0;
                rightTab.interactable = false;
                rightTab.blocksRaycasts = false;

            }
            else
            {
                rightTab.alpha = 1;
                rightTab.interactable = true;
                rightTab.blocksRaycasts = true;
            }

            if (pStats.currentHP > 0)
                health.fillAmount = ((float)pStats.currentHP / (float)pStats.maxHP);
            else
                health.fillAmount = 0;

            if (pStats.currentMP > 0)
                mana.fillAmount = ((float)pStats.currentMP / (float)pStats.maxMP);
            else
                mana.fillAmount = 0;

            if (pStats.currentExp > 0)
                exp.fillAmount = ((float)pStats.currentExp / (float)pStats.requiredExp);
            else
                exp.fillAmount = 0;

            if (pStats.target != null)
            {
                targetCG.alpha = 1;
                targetCG.blocksRaycasts = true;
                targetCG.interactable = true;
                targetName.text = pStats.target.GetComponent<enemyStats>().thisName;
                targetLevel.text = "Level " + pStats.target.GetComponent<enemyStats>().level.ToString();
                if (pStats.target.GetComponent<enemyStats>().currentHP>0) 
                    targetHealth.text = pStats.target.GetComponent<enemyStats>().currentHP + "/" + pStats.target.GetComponent<enemyStats>().maxHP;
                else
                    targetHealth.text = "0/" + pStats.target.GetComponent<enemyStats>().maxHP;
            }
            else
            {
                targetCG.blocksRaycasts = false;
                targetCG.interactable = false;
                targetCG.alpha = 0;
            }

            healthText.text = pStats.currentHP.ToString() + " / " + pStats.maxHP.ToString();
            manaText.text = pStats.currentMP.ToString() + " / " + pStats.maxMP.ToString();
            expText.text = pStats.currentExp.ToString() + " / " + pStats.requiredExp.ToString();
            levelText.text = "Level " + pStats.level.ToString();

            keybindings[0].text = "Keybinding:  " + KeyBindingManager.instance.HealthPotion.ToString();
            keybindings[1].text = "Keybinding:  " + KeyBindingManager.instance.HolyGrenade.ToString();
            keybindings[2].text = "Keybinding:  " + KeyBindingManager.instance.BlindingFlashLight.ToString();
            keybindings[3].text = "Keybinding:  " + KeyBindingManager.instance.Frenzy.ToString();
            keybindings[4].text = "Keybinding:  " + KeyBindingManager.instance.HolySmite.ToString();
            keybindings[5].text = "Keybinding:  " + KeyBindingManager.instance.ManaPotion.ToString();

            manaCosts[0].text = "Mana: " + Hero.GetComponent<HolyGrenade>().mana.ToString();
            manaCosts[1].text = "Mana: " + Hero.GetComponent<BlindingLight>().mana.ToString();
            manaCosts[2].text = "Mana: " + Hero.GetComponent<Frenzy>().mana.ToString();
            manaCosts[3].text = "Mana: " + Hero.GetComponent<HolySmite>().mana.ToString();

            for (int i = 0; i < 9; i++)
            {

                if (inv.backpack[i] != null && inv.backpack[i].type != Item.ItemType.NoType)
                {
                    backPack[i].text = inv.backpack[i].name;
                    backPackIcons[i].enabled = true;

                    if (inv.backpack[i].type == Item.ItemType.Helmet)
                    {
                        backPackIcons[i].sprite = iconSources[0];
                    }

                    if (inv.backpack[i].type == Item.ItemType.Chest)
                    {
                        backPackIcons[i].sprite = iconSources[1];
                    }

                    if (inv.backpack[i].type == Item.ItemType.Gloves)
                    {
                        backPackIcons[i].sprite = iconSources[2];
                    }

                    if (inv.backpack[i].type == Item.ItemType.Sword)
                    {
                        backPackIcons[i].sprite = iconSources[3];
                    }

                    if (inv.backpack[i].type == Item.ItemType.Pants)
                    {
                        backPackIcons[i].sprite = iconSources[4];
                    }

                    if (inv.backpack[i].type == Item.ItemType.Boots)
                    {
                        backPackIcons[i].sprite = iconSources[5];
                    }

                    if (inv.backpack[i].rarity == Item.RarityType.Common)
                        backPack[i].color = commonColor;
                    if (inv.backpack[i].rarity == Item.RarityType.Rare)
                        backPack[i].color = rareColor;
                    if (inv.backpack[i].rarity == Item.RarityType.Legendary)
                        backPack[i].color = legendaryColor;
                    if (inv.backpack[i].rarity == Item.RarityType.Ancient)
                        backPack[i].color = ancientColor;
                }
                else
                {
                    backPack[i].text = "Empty";
                    backPack[i].color = backPack[i].color = new Color(180, 180, 180);
                    backPackIcons[i].enabled = false;
                }

            }

            if (inv.character["Helmet"] != null)
            {
                equipped[0].enabled = true;
                if (inv.character["Helmet"].rarity == Item.RarityType.Common)
                {
                    equipped[0].color = commonColor;
                    
                }
                if (inv.character["Helmet"].rarity == Item.RarityType.Rare)
                    equipped[0].color = rareColor;
                if (inv.character["Helmet"].rarity == Item.RarityType.Legendary)
                    equipped[0].color = legendaryColor;
                if (inv.character["Helmet"].rarity == Item.RarityType.Ancient)
                    equipped[0].color = ancientColor;

            }
            else
                equipped[0].enabled = false;

            if (inv.character["Chest"] != null)
            {
                equipped[1].enabled = true;
                if (inv.character["Chest"].rarity == Item.RarityType.Common)
                    equipped[1].color = commonColor;
                if (inv.character["Chest"].rarity == Item.RarityType.Rare)
                    equipped[1].color = rareColor;
                if (inv.character["Chest"].rarity == Item.RarityType.Legendary)
                    equipped[1].color = legendaryColor;
                if (inv.character["Chest"].rarity == Item.RarityType.Ancient)
                    equipped[1].color = ancientColor;
            }
            else
                equipped[1].enabled = false;

            if (inv.character["Gloves"] != null)
            {
                equipped[2].enabled = true;
                if (inv.character["Gloves"].rarity == Item.RarityType.Common)
                    equipped[2].color = commonColor;
                if (inv.character["Gloves"].rarity == Item.RarityType.Rare)
                    equipped[2].color = rareColor;
                if (inv.character["Gloves"].rarity == Item.RarityType.Legendary)
                    equipped[2].color = legendaryColor;
                if (inv.character["Gloves"].rarity == Item.RarityType.Ancient)
                    equipped[2].color = ancientColor;
            }
            else
                equipped[2].enabled = false;

            if (inv.character["Sword"] != null)
            {
                equipped[3].enabled = true;
                if (inv.character["Sword"].rarity == Item.RarityType.Common)
                    equipped[3].color = commonColor;
                if (inv.character["Sword"].rarity == Item.RarityType.Rare)
                    equipped[3].color = rareColor;
                if (inv.character["Sword"].rarity == Item.RarityType.Legendary)
                    equipped[3].color = legendaryColor;
                if (inv.character["Sword"].rarity == Item.RarityType.Ancient)
                    equipped[3].color = ancientColor;
            }
            else
                equipped[3].enabled = false;

            if (inv.character["Pants"] != null)
            {
                equipped[4].enabled = true;
                if (inv.character["Pants"].rarity == Item.RarityType.Common)
                    equipped[4].color = commonColor;
                if (inv.character["Pants"].rarity == Item.RarityType.Rare)
                    equipped[4].color = rareColor;
                if (inv.character["Pants"].rarity == Item.RarityType.Legendary)
                    equipped[4].color = legendaryColor;
                if (inv.character["Pants"].rarity == Item.RarityType.Ancient)
                    equipped[4].color = ancientColor;

            }
            else
                equipped[4].enabled = false;

            if (inv.character["Boots"] != null)
            {
                equipped[5].enabled = true;
                if (inv.character["Boots"].rarity == Item.RarityType.Common)
                    equipped[5].color = commonColor;
                if (inv.character["Boots"].rarity == Item.RarityType.Rare)
                    equipped[5].color = rareColor;
                if (inv.character["Boots"].rarity == Item.RarityType.Legendary)
                    equipped[5].color = legendaryColor;
                if (inv.character["Boots"].rarity == Item.RarityType.Ancient)
                    equipped[5].color = ancientColor;

            }
            else
                equipped[5].enabled = false;
        }

        if (!Camera.main.GetComponent<mouseInput>().showLoot)
        {
            lootWindow.alpha = 0;
            lootWindow.interactable = false;
            lootWindow.blocksRaycasts = false;
        }



    }

    void reduceCooldown()
    {
        for (int i = 0; i < 6; i++)
        {
            if (cds[i] > 0)
                cds[i]--;
            if (cds[i] == 0)
            {
                cooldownsText[i].text = "";
                cooldownImages[i].enabled = false;
            }
            else
            {
                cooldownsText[i].text = cds[i].ToString();
            }
        }
    }

    public void StartCooldown(int i, int cd)
    {
        cds[i] = cd;
        cooldownsText[i].text = cd.ToString();
        cooldownImages[i].enabled = true;
    }

    public void CloseWindow()
    {
        lootWindow.alpha = 0;
        lootWindow.interactable = false;
        lootWindow.blocksRaycasts = false;
        Camera.main.GetComponent<mouseInput>().showLoot = false;
    }

    public void ShowInventory()
    {
        if (!Camera.main.GetComponent<keyboardInput>().showInventory)
            Camera.main.GetComponent<keyboardInput>().showInventory = true;
        else
            Camera.main.GetComponent<keyboardInput>().showInventory = false;
    }

    public void ShowMenu()
    {
        settings.enabled = true;
        Time.timeScale = 0;
        canvas.enabled = false;
        Camera.main.GetComponent<keyboardInput>().showMenu = true;
    }

    public void HoverSkill(int i)
    {

        skillTooltips[i].alpha = 1;
        skillTooltips[i].interactable = true;
        skillTooltips[i].blocksRaycasts = true;

        showTooltip[i] = true;
        hoverImages[i].enabled = true;

    }

    public void StopHovering(int i)
    {
        skillTooltips[i].alpha = 0;
        skillTooltips[i].interactable = false;
        skillTooltips[i].blocksRaycasts = false;

        showTooltip[i] = false;
        hoverImages[i].enabled = false;
    }

}
