﻿using UnityEngine;
using System.Collections;

public class enemyFloatingHealth : MonoBehaviour
{

    public TextMesh text;
    public float speed;
    public Vector3 direction;
    public Color color;
    public int health, maxHealth;
    public enemyStats eStats;

    void Start()
    {

        direction = Vector3.up*3;

        color = Color.black;
        eStats = GetComponentInParent<enemyStats>();
        text = GetComponent<TextMesh>();
        health = eStats.currentHP;
        maxHealth = eStats.maxHP;
        text.text = health.ToString() + " / " + maxHealth.ToString();
        //transform.position = transform.parent.position + Vector3.up * 5;
    }

    void Update()
    {
        health = eStats.currentHP;
        if (health > 0)
        {
            text.text = health.ToString() + " / " + maxHealth.ToString();
            
            transform.LookAt(2 * transform.position - (Camera.main.transform.position));    
        }
        if (health < 0)
            text.text = "0/ " + maxHealth.ToString();
    }
}
