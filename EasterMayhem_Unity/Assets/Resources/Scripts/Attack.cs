﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour
{

    private Animator animator, heroAnimator;
    private EnemyAI me;
    public bool hit;

    void Start()
    {
        animator = GetComponent<Animator>();
        me = GetComponent<EnemyAI>();
        heroAnimator = me.Hero.GetComponent<Animator>();
    }

    void Update()
    {
        hit = false;
        if (Vector3.Distance(me.Hero.transform.position, transform.position) < 2f && me.Hero.GetComponent<playerStats>().currentHP > 0 && GetComponent<enemyStats>().dead == false && 
            heroAnimator.GetComponent<playerStats>().dead == false)
        {
            transform.LookAt(me.Hero.transform.position);
            hit = true;
            animator.SetTrigger("Attack");
        }
    }
}
