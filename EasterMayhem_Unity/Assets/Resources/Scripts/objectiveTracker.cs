﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class objectiveTracker : MonoBehaviour {

    public Test story;
    public bool objective1, objective2, objective3;
    public Text[] obj;

    private static objectiveTracker _instance;
    public static objectiveTracker instance
    {
        get
        {
            if (_instance == null)
                _instance = GameObject.FindObjectOfType<objectiveTracker>();
            return _instance;
        }
    }

	void Start () {
        obj[0].text = "Blind Bunny killed: 0/1";
        obj[1].text = "Hellish Bunny killed: 0/1";
        obj[2].text = "";
	}
	
	
	void Update () {
	    if(objective1)
        {
            obj[0].text = "Blind Bunny killed: 1/1"; 
        }
        if(objective2)
        {
            obj[1].text = "Hellish Bunny killed: 1/1";
        }
        if(objective1 && objective2 && obj[0] && obj[1])
        {
            GameObject.Destroy(obj[0].gameObject);
            GameObject.Destroy(obj[1].gameObject);
            obj[2].text = "Open the Hellgates 0/1";
        }

        if (objective3)
        {
            objective3 = false;
            story.getToFirstLine();
            story.disableUI();
            story.triggerScene3 = true;  
        }
	}
}
