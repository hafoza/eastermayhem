﻿using UnityEngine;
using System.Collections;

public class keyboardInput : MonoBehaviour {

    public bool showInventory,toggled,toggled3;
    public playerStats pStats;
    public UIControl ui;
    public string inv;
    public float rawFps,fps,maxFps,minFps,avgFps;
    public bool showFPS, toggled2, showMenu, inStory;
    public int qty;
	
	void Start () {
        pStats = GameObject.FindGameObjectWithTag("Jesus").GetComponent<playerStats>();
        minFps = 60;
        maxFps = 0;
        showInventory = false;
        InvokeRepeating("updateFps", 0.0F, 1.0F);
        
	}
	
	
	void Update () {

        toggled = false;
        toggled2 = false;
        toggled3 = false;
        

 


        if (showMenu == true && Input.GetKeyDown(KeyCode.Escape))
        {
            showMenu = false;
            toggled3 = true;
            
        }

        if (showMenu == false && Input.GetKeyDown(KeyCode.Escape) && !showInventory && !toggled3)
        {
            showMenu = true;
            

        }

        if (showInventory == true && (Input.GetButtonDown(inv) || Input.GetKeyDown(KeyCode.Escape)))
        {
            showInventory = false;
            toggled = true;
        }

        if (showInventory == false && Input.GetButtonDown(inv) && !toggled)
        {
            showInventory = true;
        }

        if (showFPS == true && Input.GetKeyDown(KeyCode.BackQuote))
        {
            showFPS = false;
            toggled2 = true;
        }
        if (showFPS == false && Input.GetKeyDown(KeyCode.BackQuote) && !toggled2)
        {
            showFPS = true;
        }

        if (showFPS == true)
        { 
            qty ++;
            rawFps = (1.0F/Time.deltaTime);
            if (rawFps < minFps)
                minFps = rawFps;
            if (rawFps > maxFps)
                maxFps = rawFps;

            avgFps += (rawFps - avgFps) / qty;
        }
        if (showInventory == false)
        {
            ui.inventoryCG.alpha = 0;
            ui.inventoryCG.blocksRaycasts = false;
            ui.inventoryCG.interactable = false;
        }
        else
        {
            ui.inventoryCG.alpha = 1;
            ui.inventoryCG.blocksRaycasts = true;
            ui.inventoryCG.interactable = true;
        }

        if (showMenu == true && !inStory)
        {
            ui.ShowMenu();
        }

        if (showMenu == false && !inStory)
        {
            ui.settings.GetComponent<SettingsUI>().resume();
            
        }

        if (Input.GetKeyDown(KeyCode.LeftBracket))
        {
            pStats.increaseExperience(5000);
        }
        if (Input.GetKeyDown(KeyCode.O))
        {
            pStats.gameObject.GetComponent<NavMeshAgent>().Warp(new Vector3(30, 1, 170));
            pStats.gameObject.GetComponent<NavMeshAgent>().SetDestination(new Vector3(30, 1, 170));
        }
        if (Input.GetKeyDown(KeyCode.P))
        {
            pStats.gameObject.GetComponent<NavMeshAgent>().Warp(new Vector3(150, 1, 90));
            pStats.gameObject.GetComponent<NavMeshAgent>().SetDestination(new Vector3(150, 1, 90));
        }
	}

    void updateFps()
    {
        fps = rawFps;
    }

    void OnGUI()
    {
        if (showFPS)
        {
            GUILayout.Label("FPS(updated every second): " + fps.ToString());
            GUILayout.Label("FPS(updated every frame): " + rawFps.ToString());
            GUILayout.Label("FPS(average): " + avgFps.ToString());

            GUILayout.Label("Min FPS: " + minFps.ToString());
            GUILayout.Label("Max FPS: " + maxFps.ToString());
        }
    }

}
