﻿using UnityEngine;
using System.Collections;

public class EnemySpawner : MonoBehaviour {

    public GameObject enemySource, spawnedEnemy;
    public float spawnTime, respawnPeriod;
    public int level;
    public float curTime,deathTime;

    void Start()
    {
        spawn(0);
    
    }

    public void spawn(float currentTime)
    {
        spawnTime = currentTime;

        spawnedEnemy = (GameObject)GameObject.Instantiate(enemySource, transform.position, transform.rotation);
        spawnedEnemy.GetComponent<enemyStats>().spawn(level,gameObject);
    }


	void Update () 
    {
        curTime = Time.time;

        if (!spawnedEnemy)
        {
            if ((Time.time - deathTime) >= respawnPeriod)
                spawn(curTime);
        }
        
	}

    void OnDrawGizmos()
    {
        Gizmos.DrawCube(transform.position, Vector3.one);
    }
}
