﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class enemyStats : MonoBehaviour 
{
    [Range(1.0f, 2.0f)]
    public float movementSpeed;
    public int armor, currentHP;
    public int maxHP, level;    
    public int damage;
    public int expGiven;
    public float range;
    [Range(1, 3)]
    public int itemsDropped;
    [HideInInspector]
    public bool dead;
    public string thisName;
    public GameObject spawnedBy, killedBy;
    public GameObject loot;
    public Animator animator;
    public GameObject damageTextSource;
    public int id;

    public void spawn (int lvl, GameObject spawner)
    {
        id = assignRandomID();
        gameManager.Instance.assignEnemyID(id,name);
        gameObject.name = "Enemy id#" + id.ToString(); 

        spawnedBy = spawner;

        level = lvl;
        maxHP = currentHP = (int)(maxHP * level);
        armor = (int)(armor * level);
        damage = (int)(damage * level);
    }

    int assignRandomID()
    {
        int tempID = Random.Range(0, 1000);

        if (gameManager.Instance.checkID(tempID))
            return tempID;
        else
            return assignRandomID();
        
    }

    public void modifyHP(int modifier, int type)
    {
        if (type == -1)
        {
            currentHP -= (int)((modifier * ((100 - (int)(Mathf.Log(armor) * 10))) / 100));
        }

        if (type == 1)
        {
            currentHP += modifier;

        }

        GameObject damageText = (GameObject)GameObject.Instantiate(damageTextSource, transform.position + (Vector3.up * 5), Quaternion.identity);
        damageText.GetComponent<floatingDamageText>().type = 1;
        damageText.GetComponent<floatingDamageText>().damage = ((int)((modifier * (100 - (int)(Mathf.Log(armor) * 10))) / 100)).ToString();

    }

    public void die(GameObject killer)
    {
        if (!dead)
        {
            dead = true;
            spawnedBy.GetComponent<EnemySpawner>().deathTime = Time.time;
            killedBy = killer;
            playerStats pStats = killer.GetComponent<playerStats>();
            int exp = (int)((1 - (0.25F * (pStats.level - level))) * expGiven);
            if (exp < 0)
                exp = 0;
            pStats.increaseExperience(exp);
            animator.SetTrigger("Dead");

        }


    }

    void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (animator.GetCurrentAnimatorStateInfo(0).fullPathHash == Animator.StringToHash("Movement.Dieded") && dead)
        {
            GameObject temp = (GameObject)Instantiate(loot, transform.position + Vector3.up - (transform.TransformDirection(Vector3.forward)), Quaternion.identity);
            temp.name = "Loot #" + id.ToString();

            GameObject.Destroy(gameObject);
        }
    }
}
