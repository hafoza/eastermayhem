﻿using UnityEngine;
using System.Collections;

public class mouseInput : MonoBehaviour {


    public Vector3 inputWorldPosition, inputHoverPosition;
    private Camera mainCamera;
    private Ray ray;
    private RaycastHit hit;
    public Transform target;
    public GameObject player,enemy;
    public bool moving, attacking, showLoot;
    public BasicAttack ba;

	void Awake () 
    {
        mainCamera = Camera.main;
        inputWorldPosition = target.position + new Vector3(1,0,1);
        inputHoverPosition = target.position + new Vector3(1, 0, 1);
        moving = attacking = false;
	}
	
    
    void Update () 
    {

        if (!enemy)
            attacking = false;

        if (!Input.GetMouseButton(0))
        {
            ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit, 70.0F))
            {
                if (hit.collider.gameObject.tag == "Ground")
                {
                    inputHoverPosition = hit.point;
                }
            }
        }
        if (Input.GetMouseButtonDown(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {

            ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit, 70.0F))
            {
                if (hit.collider.gameObject.tag == "Ground")
                {
                    inputWorldPosition = hit.point;
                    if (Input.GetButton("Stay"))
                        moving = false;
                    else
                        moving = true;
                }
                else if (hit.collider.gameObject.GetComponentInParent<enemyStats>())
                {
                    player.GetComponent<playerStats>().target = hit.collider.GetComponentInParent<enemyStats>().gameObject;
                    attacking = true;
                }
                else if (hit.collider.gameObject.tag == "Loot" && Vector3.Distance(hit.point, player.transform.position) <= 3.5F)
                { 
                    showLoot = true;
                    
                }
                else if (hit.collider.gameObject.tag == "Loot")
                {
                    inputHoverPosition = inputWorldPosition = hit.point;
                    player.GetComponent<playerStats>().selectedLoot = hit.collider.gameObject;
                }
            }

            
        }

       
        if (Input.GetMouseButton(0) && !UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
        {
            

            ray = mainCamera.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray.origin, ray.direction, out hit, 70.0F))
            {
                if (hit.collider.gameObject.tag == "Ground")
                {
                    if (!Input.GetButton("Stay"))
                        moving = true;
                }
                else if (hit.collider.gameObject.GetComponentInParent<enemyStats>())
                    if (!Input.GetButton("Stay"))
                        attacking = true;

                if (hit.collider.gameObject.tag == "Ground" && (moving || Input.GetButton("Stay") || !player.GetComponent<HolyGrenade>().throwingGrenade))
                {
                    
                    inputWorldPosition = hit.point;
                }
                else if (hit.collider.gameObject.GetComponentInParent<enemyStats>() && attacking)
                    {
                        ba = player.GetComponent<BasicAttack>();
                        enemy = hit.collider.GetComponentInParent<enemyStats>().gameObject;
                        player.GetComponent<playerStats>().target = enemy;

                    }

                    else if (hit.collider.gameObject.tag == "Loot" && Vector3.Distance(hit.point, player.transform.position) <= 3.5f)
                    {
                        showLoot = true;
                        player.GetComponent<playerStats>().selectedLoot = hit.collider.gameObject;
                    }
                    else if (hit.collider.gameObject.tag == "Loot")
                    {
                        inputHoverPosition = inputWorldPosition = hit.point;
                        player.GetComponent<playerStats>().selectedLoot = hit.collider.gameObject;
                    }
            }
        }

        if (attacking && enemy.GetComponent<enemyStats>().dead == false)
        {
            inputWorldPosition = enemy.transform.position + (-(enemy.transform.position - player.transform.position).normalized * 0.5F);
            if (Vector3.Distance(player.transform.position, enemy.transform.position) <= player.GetComponent<NavMeshAgent>().stoppingDistance + 2f)
            {
                ba.animator.SetTrigger("Attack");
                ba.hit = true;
            }
        }

        if (Input.GetMouseButtonUp(0) || Input.GetButton("Stay"))
        {
            moving = false;
            attacking = false;
        }


	}

    void OnDrawGizmos()
    {
        Gizmos.DrawRay(ray.origin, ray.direction*50);
        Gizmos.DrawSphere(inputWorldPosition, 0.5F);
    }
}
