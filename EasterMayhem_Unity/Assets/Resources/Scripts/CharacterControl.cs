﻿using UnityEngine;
using System.Collections;

public class CharacterControl : MonoBehaviour {

    private NavMeshAgent agent;
    private mouseInput input;
    private Animator animator;
    private playerStats player;
    private float speedFactor, targetSpeedFactor;
    public string stay;

    
    public Quaternion finalRotation,tempRot,initialRotation;
    public Vector3 finalDestination,tempDest;
    public bool overrideRotation, overrideDestination;

	void Start () 
    {
        agent = GetComponent<NavMeshAgent>();
        input = Camera.main.GetComponent<mouseInput>();
        animator = GetComponent<Animator>();
        agent.stoppingDistance = 0.2f;
        player = GetComponent<playerStats>();
        tempRot = Quaternion.identity;
    }
	
	void Update () 
    {

        finalRotation = initialRotation;

        if (!Input.GetButton(stay))
            finalDestination = input.inputWorldPosition;

        if (agent.hasPath)
        {
            targetSpeedFactor = Mathf.Clamp01(agent.velocity.magnitude);
            speedFactor = Mathf.Lerp(speedFactor, targetSpeedFactor, Time.deltaTime * 4f);
            
        }
        else
        {
            targetSpeedFactor = 0;
            speedFactor = Mathf.Lerp(speedFactor, targetSpeedFactor, Time.deltaTime * 10f);
        }

        if (agent.remainingDistance < 1f && agent.hasPath)
        {
            speedFactor = 0f;
        }


        animator.SetFloat("Speed", speedFactor);

        if (player.target != null && Vector3.Distance(input.inputWorldPosition, input.enemy.transform.position) < 1.5F)
        {

            Quaternion lookRotation = Quaternion.LookRotation(((input.inputWorldPosition - new Vector3(0, input.inputWorldPosition.y, 0))
                - (transform.position - new Vector3(0, transform.position.y, 0))).normalized);
            overrideRot(lookRotation);
        }
        
        if (Input.GetButton(stay))
        {
            overrideDest(transform.position);
            Quaternion lookRotation = Quaternion.LookRotation(((input.inputWorldPosition - new Vector3(0, input.inputWorldPosition.y, 0)) 
                - (transform.position - new Vector3(0,transform.position.y,0))).normalized);
            overrideRot(lookRotation);
            
        }

       

        if (overrideDestination)
        {
            finalDestination = tempDest;
        }

        if (overrideRotation)
        {
            finalRotation = tempRot;
        }


        if (!overrideRotation)
        {
            transform.rotation = finalRotation;
        }
        else
        {
            transform.rotation = tempRot;
        }

        agent.SetDestination(finalDestination);

        if (Input.GetButtonUp(stay) || GetComponent<HolyGrenade>().throwingGrenade)
        {
            input.inputWorldPosition = transform.position + transform.TransformDirection(Vector3.forward);

        }

        GetComponent<HolyGrenade>().throwingGrenade = false;
	}

    void LateUpdate()
    {
        if (overrideRotation)
        {
            transform.rotation = tempRot;
        }
        overrideRotation = false;
        overrideDestination = false;

    }

    public void overrideDest(Vector3 dest)
    {
        tempDest = dest;
        overrideDestination = true;
    }

    public void overrideRot(Quaternion rot)
    {
        tempRot = rot;
        overrideRotation = true;
        
    }


    void OnAnimatorMove()
    {
        float animation_position;

        animation_position = (animator.deltaPosition.magnitude > 0.1f) ? animator.deltaPosition.magnitude : 0.1f;

        if (agent.remainingDistance > 1f && Time.timeScale != 0)
            agent.velocity = Vector3.Lerp(agent.velocity, agent.desiredVelocity.normalized * (player.movementSpeed + 0.11f) * (animation_position / Time.deltaTime), Time.deltaTime * 4f);
        else
            agent.velocity = Vector3.zero;
       
        

        if (agent.desiredVelocity != Vector3.zero)
        {
            Quaternion lookRotation = Quaternion.LookRotation(agent.desiredVelocity);
            initialRotation = Quaternion.Slerp(transform.rotation, lookRotation, agent.angularSpeed * Time.deltaTime);
            
        }
    }
}
