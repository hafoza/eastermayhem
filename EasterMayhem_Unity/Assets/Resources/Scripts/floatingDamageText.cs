﻿using UnityEngine;
using System.Collections;

public class floatingDamageText : MonoBehaviour {

    public TextMesh text;
    public float speed, startTime, wantedTime;
    public string damage;
    public Vector3 direction;
    public Color color;
    public int type;

    void Start()
    {

        direction = Vector3.up;
        
        if (type == 0)
            color = Color.red;
        if (type == 1)
            color = Color.black;
        if (type == 2)
            color = Color.yellow;
        
        text = GetComponent<TextMesh>();
        startTime = Time.time;
        text.text = damage;
    }

	void Update () {

        color.a = Mathf.Lerp(1, 0, (Time.time - startTime) / wantedTime);
       // mr.material.color.a = Mathf.Lerp(1, 0, Time.deltaTime * speed);
        text.color = color;

        transform.Translate(direction * speed * Time.deltaTime);

        if (color.a == 0)
            GameObject.Destroy(gameObject);

        transform.LookAt(2 * transform.position - Camera.main.transform.position);

        //transform.rotation = Quaternion.FromToRotation(transform.position, Camera.main.transform.position);

	}
}
