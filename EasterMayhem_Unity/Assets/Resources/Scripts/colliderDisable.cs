﻿using UnityEngine;
using System.Collections;

public class colliderDisable : MonoBehaviour {

	
	void Start () {
        GetComponent<SphereCollider>().enabled = false;
        StartCoroutine(wait());
	}

    IEnumerator wait()
    {
        yield return new WaitForSeconds(1.0F);

        GetComponent<SphereCollider>().enabled = true;  
    }
	
}
