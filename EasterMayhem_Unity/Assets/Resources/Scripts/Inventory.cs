﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Inventory : MonoBehaviour{

    public Dictionary<string, Item> character = new Dictionary<string, Item>();
    public Item[] backpack = new Item[10];
    public int i;
    public playerStats stats;

    void Start()
    {
        stats = GetComponent<playerStats>();
    }

    public Inventory()
    {
        character.Add("Helmet", null);
        character.Add("Chest", null);
        character.Add("Pants", null);
        character.Add("Gloves", null);
        character.Add("Boots", null);
        character.Add("Sword", null);

    } 

    public void InitializeBackpack()
    {
        for (i = 0; i < 10; i++)
            backpack[i] = null;
    }


    public bool AddItem(Item itemToAdd)
    {
        for (i = 0; i < 9; i++)
            if(backpack[i] == null)
            {
                backpack[i] = itemToAdd;
                backpack[i].backpackIndex = i;
                return true;
            }
        return false;
    }

    #region Destroy Item // Hope this works
    public void DestroyItem(ref Item itemToDestroy)
    {
      
        backpack[itemToDestroy.backpackIndex] = null;
        itemToDestroy = null;
    }

    public void DestroyItemEquipped(Item itemToDestroy)
    {
        if (itemToDestroy.backpackIndex == -1)
            character[itemToDestroy.type.ToString()] = null;
        itemToDestroy = null;
    }

    #endregion

    public void Equip(ref Item itemToEquip)
    {
        Item temp;

        if (character[itemToEquip.type.ToString()] == null)
        {
            character[itemToEquip.type.ToString()] = itemToEquip;
            character[itemToEquip.type.ToString()].backpackIndex = -1;
            ItemStatsToCharacter(itemToEquip, true); // Add
            //itemToEquip = null;
            if(itemToEquip.backpackIndex!=-1)
                backpack[itemToEquip.backpackIndex] = null;
            itemToEquip.backpackIndex = -1;
            itemToEquip = null;
        }
        else
        {
            int x = itemToEquip.backpackIndex;
            //itemToEquip.backpackIndex = -1;
            temp = character[itemToEquip.type.ToString()];
            ItemStatsToCharacter(temp, false); // Substract old
            character[itemToEquip.type.ToString()] = itemToEquip;
            ItemStatsToCharacter(itemToEquip, true); // Add new
            backpack[x] = temp;
            backpack[x].backpackIndex = x;
            
            
        }
    }

    public bool Unequip(Item.ItemType slot)
    {
        for (i = 0; i < 10; i++ )
            if (backpack[i] == null)
            {
                backpack[i] = character[slot.ToString()];
                backpack[i].backpackIndex = i;
                ItemStatsToCharacter(character[slot.ToString()], false); // Substract
                character[slot.ToString()] = null;
                return true;
            }
        return false;
    }

    #region Add Or Remove Item stats from Character
    private void ItemStatsToCharacter(Item item, bool add)
    {
        //true for adding/ false for substracting
        int mod;
        mod = (add) ? 1 : -1;

        stats.movementSpeed += mod * item.movementSpeed;
        stats.regenHP += mod * item.healthRegen;
        stats.regenMP += mod * item.manaRegen;
        stats.constitution += mod * item.constitution;
        stats.maxHP += mod * 10 * item.constitution;
        stats.strength += mod * item.strength;
        stats.wisdom += mod * item.wisdom;
        stats.maxMP += mod * 5 * item.wisdom;
        stats.armor += mod * item.armor;
    }
    #endregion

    #region Swap Items in inventory
    public void SwapItems(int index1, int index2)
    {
        Item temp;
        temp = backpack[index1];
        backpack[index1] = backpack[index2];
        backpack[index2] = temp;
    }
    #endregion

}
