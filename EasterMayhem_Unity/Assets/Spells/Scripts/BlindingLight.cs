﻿using UnityEngine;
using System.Collections;

public class BlindingLight : MonoBehaviour
{
    public int CD, mana, duration;
    public bool onCD = false;
    private Animator animator;
    public GameObject particle;
    private GameObject obj;
    private Collider[] Colliders;

    // Use this for initialization
    void Start()
    {

        animator = GetComponent<Animator>();

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyBindingManager.instance.BlindingFlashLight) && !onCD && GetComponent<playerStats>().currentMP >= mana)
        {
            gameManager.Instance.ui.StartCooldown(2, CD);
            animator.SetTrigger("Blind");
            onCD = true;
            GetComponent<playerStats>().modifyMP(mana, -1);
            Colliders = Physics.OverlapSphere(transform.position, 7);
            obj = (GameObject)Instantiate(particle, transform.position + Vector3.up * 2f, Quaternion.identity);
            obj.transform.parent = gameObject.transform;

            foreach (Collider colliders in Colliders)
            {
                if (colliders.GetComponentInParent<EnemyAI>())
                {
                    colliders.GetComponentInParent<Attack>().enabled = false;
                    colliders.GetComponentInParent<Animator>().SetFloat("Speed", 0);
                    colliders.GetComponentInParent<Animator>().SetTrigger("Blinded");
                    colliders.GetComponentInParent<EnemyAI>().enabled = false;
                    colliders.GetComponentInParent<NavMeshAgent>().destination = colliders.GetComponentInParent<NavMeshAgent>().gameObject.transform.position;
                }
            }

            Invoke("ReEnable", duration);

            GameObject.Destroy(obj, 6);
        }
    }

    void ReEnable()
    {
            foreach (Collider colliders in Colliders)
            {
                if(colliders != null)
                    if (colliders.GetComponentInParent<EnemyAI>())
                    {
                        colliders.GetComponentInParent<EnemyAI>().enabled = true;
                        colliders.GetComponentInParent<Attack>().enabled = true;
                        colliders.GetComponentInParent<EnemyAI>().foundPlayer = false;
                    }
            }
        onCD = false;
    }
}
