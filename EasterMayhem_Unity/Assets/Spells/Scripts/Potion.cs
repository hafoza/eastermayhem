﻿using UnityEngine;
using System.Collections;

public class Potion : MonoBehaviour
{
    public int cdHealth, cdMana;
    [Range (0.0f, 1.0f)]
    public float healthPercentage, manaPercentage;
    private bool onCDHealth = false, onCDMana = false;
    public GameObject particle;
    private GameObject obj;
    private playerStats player;
    private Collider[] Colliders;

    // Use this for initialization
    void Start()
    {
        player = GetComponent<playerStats>();
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(KeyBindingManager.instance.HealthPotion) && !onCDHealth)
        {
            gameManager.Instance.ui.StartCooldown(0, cdHealth);
            onCDHealth = true;
            //player.modifyHP((int)(player.maxHP * healthPercentage), 1);
            player.currentHP += (int)(player.maxHP * healthPercentage);
            Invoke("RefreshCDHealth", cdHealth);
        }

        if (Input.GetKey(KeyBindingManager.instance.ManaPotion) && !onCDMana)
        {
            gameManager.Instance.ui.StartCooldown(5, cdMana);
            onCDMana = true;
            player.modifyMP((int)(player.maxMP * manaPercentage), 1);
            Invoke("RefreshCDMana", cdMana);
        }
    }

    void RefreshCDHealth()
    {
        onCDHealth = false;
    }

    void RefreshCDMana()
    {
        onCDMana = false;
    }
}
