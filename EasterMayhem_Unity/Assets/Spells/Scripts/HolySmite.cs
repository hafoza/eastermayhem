﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HolySmite : MonoBehaviour
{
    public GameObject explosion;
    private GameObject obj;
    public int CD, damage, mana;
    private bool onCD = false;
    private Animator animator;
    private Collider[] Colliders;
    private playerStats player;
    private List<int> ids = new List<int>();

    
    void Start()
    {
        animator = GetComponent<Animator>();
        player = GetComponent<playerStats>();
    }

    void Update()
    {


        if (Input.GetKey(KeyBindingManager.instance.HolySmite) && !onCD && GetComponent<playerStats>().currentMP >= mana)
        {
            gameManager.Instance.ui.StartCooldown(4, CD);
            animator.SetTrigger("HolySmite");

            player.modifyMP(mana, -1);
            onCD = true;
            obj = (GameObject)GameObject.Instantiate(explosion, transform.position + Vector3.up, Quaternion.identity);
            obj.transform.parent = transform;

            Colliders = Physics.OverlapSphere(transform.position, 8);
            ids.Clear();
            foreach (Collider colliders in Colliders)
            {
                if (!ids.Contains(colliders.gameObject.GetInstanceID()) && colliders.GetComponentInParent<enemyStats>() &&  colliders.tag == "HitBox")
                {
                    ids.Add(colliders.gameObject.GetInstanceID());
                    colliders.GetComponentInParent<enemyStats>().modifyHP((int)(damage + (player.wisdom * 5)),-1);
                    if (colliders.GetComponentInParent<enemyStats>().currentHP <= 0)
                    {
                        colliders.GetComponentInParent<enemyStats>().die(player.gameObject);

                    }
                }
            }

            Invoke("ReEnable", CD);
            GameObject.Destroy(obj, 2);
        }
    }

    void ReEnable()
    {
        
        onCD = false;
    }

}
