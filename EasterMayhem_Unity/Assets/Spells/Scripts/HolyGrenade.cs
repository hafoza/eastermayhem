﻿using UnityEngine;
using System.Collections;

public class HolyGrenade : MonoBehaviour {
    [HideInInspector]
    public mouseInput input;
    public GameObject grenade;
    [HideInInspector]
    public GameObject projectile;
    [HideInInspector]
    public Vector3 force, location, inputSource;
    public int CD, damage, mana;
    private bool onCD = false;
    public Transform grenade_loc;
    private Animator animator;
    public Quaternion lookRotation;
    public bool throwingGrenade;

	// Use this for initialization
	void Start () {

        input = Camera.main.GetComponent<mouseInput>();
        animator = GetComponent<Animator>();

	}

	// Update is called once per frame
	void Update () {


        if (Input.GetKey(KeyBindingManager.instance.HolyGrenade) && !onCD && !input.moving && GetComponent<playerStats>().currentMP >= mana)
        {
            gameManager.Instance.ui.StartCooldown(1, CD);

            throwingGrenade = true;
            onCD = true;
            animator.SetTrigger("HolyGrenade");

            if (!Input.GetMouseButton(0))
            {
                inputSource = input.inputHoverPosition;
            }
            else
                inputSource = input.inputWorldPosition;

            input.inputWorldPosition = inputSource;
            
            GetComponent<CharacterControl>().overrideDest(transform.position);

            lookRotation = Quaternion.LookRotation(((input.inputWorldPosition - new Vector3(0, inputSource.y, 0))
                    - (transform.position - new Vector3(0, transform.position.y, 0))).normalized);
           
            GetComponent<CharacterControl>().overrideRot(lookRotation);

            GetComponent<playerStats>().modifyMP(mana, -1);

            Invoke("cor", 0.1f);



            projectile = (GameObject)Instantiate(grenade, grenade_loc.position + Vector3.up, Quaternion.Euler(0,0,0));

            force.y = Vector3.Distance(transform.position, input.inputWorldPosition);
            force.x = input.inputWorldPosition.x - projectile.transform.position.x;
            force.z = input.inputWorldPosition.z - projectile.transform.position.z;

            force = force.normalized * Mathf.Sqrt(8f * Vector3.Distance(transform.position, input.inputWorldPosition)); 
            projectile.GetComponent<Rigidbody>().velocity = force;

            projectile.GetComponent<Rigidbody>().AddTorque(180, 20, 20);

            throwingGrenade = false;

            Invoke("Wait", CD);
           
        }
	}

    

    void cor()
    {
        input.inputWorldPosition = transform.position + transform.TransformDirection(Vector3.forward/10); 
    }

    void Wait()
    {
        onCD = false;
        
    }

}
