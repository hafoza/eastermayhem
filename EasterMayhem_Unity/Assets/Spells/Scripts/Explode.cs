﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Explode : MonoBehaviour {

    public GameObject particle, obj;
    private bool exploded = false;
    private List<int> ids = new List<int>();
    private GameObject jesus;
    private HolyGrenade spell;

	// Use this for initialization
	void Start () {
        jesus = GameObject.FindGameObjectWithTag("Jesus");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider otherCollider)
    {

        if (otherCollider.tag == "Ground" || otherCollider.tag == "Hitbox" && !exploded)
        {
            exploded = true;
            Collider[] Colliders = Physics.OverlapSphere(transform.position, 8);
            //gameObject.GetComponent<MeshRenderer>().enabled = false;
            obj = (GameObject)Instantiate(particle, transform.position, Quaternion.identity);
            playerStats pStats = jesus.GetComponent<playerStats>();
            ids.Clear();
            foreach(Collider collider in Colliders)
            {
                if (!ids.Contains(collider.gameObject.GetInstanceID()) && collider.GetComponentInParent<enemyStats>() && collider.tag == "HitBox")
                {
                    spell = jesus.GetComponent<HolyGrenade>();
                    ids.Add(collider.gameObject.GetInstanceID());
                    enemyStats eStats = collider.GetComponentInParent<enemyStats>();
                    // ModifyP
                    eStats.modifyHP((int)(spell.damage + 4 * pStats.wisdom), -1);

                    eStats.gameObject.GetComponent<EnemyAI>().foundPlayer = true;

                    if (eStats.currentHP <= 0)
                        eStats.die(pStats.gameObject);
                }
            }

            
            GameObject.Destroy(obj, 3);
            GameObject.Destroy(gameObject);
                
        }

    }

}
