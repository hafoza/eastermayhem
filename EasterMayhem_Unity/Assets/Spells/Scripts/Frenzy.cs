﻿using UnityEngine;
using System.Collections;

public class Frenzy : MonoBehaviour
{
    public int CD, mana, duration;
    [Range (0f, 0.5f)]
    public float heal;
    public int armor;
    public int health_regen;
    public int strength;
    [Range (0.0f, 0.5f)]
    public float ms;
    private bool onCD = false;
    private Animator animator;
    public GameObject particle;
    private GameObject obj;
    private Collider[] Colliders;
    private playerStats player;
    
    void Start()
    {

        animator = GetComponent<Animator>();
        player = GetComponent<playerStats>();

    }

    void Update()
    {

        if (Input.GetKey(KeyBindingManager.instance.Frenzy) && !onCD && GetComponent<playerStats>().currentMP >= mana)
        {
            gameManager.Instance.ui.StartCooldown(3, CD);
            animator.SetTrigger("Frenzy");
            onCD = true;
            player.modifyMP(mana, -1);
            obj = (GameObject)Instantiate(particle, transform.position + Vector3.up * 2f, Quaternion.identity);
            obj.transform.parent = gameObject.transform;

            player.currentHP += (int)(player.maxHP * heal);
            player.armor += armor;
            player.regenHP += health_regen;
            player.strength += strength;
            player.movementSpeed += ms;
            

            Invoke("Disable", duration);
            GameObject.Destroy(obj, duration + 1);
        }
    }

    void Disable()
    {
        animator.SetTrigger("FrenzyExit");
        player.armor -= armor;
        player.regenHP -= health_regen;
        player.strength -= strength;
        player.movementSpeed -= ms;
        onCD = false;
    }
}
