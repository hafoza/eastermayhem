﻿using UnityEngine;
using System.Collections;

public class dialogueTrigger : MonoBehaviour {

    public Test story;
    public Collider thisCollider;
    public GameObject bugsSpawner;

    void Start()
    {
        thisCollider = GetComponent<Collider>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<playerStats>() && objectiveTracker.instance.objective1 && objectiveTracker.instance.objective2)
        {
            story.disableUI();
            story.getToFirstLine();
            story.triggerScene2 = true;
            bugsSpawner.SetActive(true);
            bugsSpawner.GetComponent<EnemySpawner>().enabled = true;
            

            thisCollider.isTrigger = false;
        }
    }
	
	
	void Update () {
	
	}
}
