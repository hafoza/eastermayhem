﻿using UnityEngine;
using System.Collections;

public class AILavaPool : MonoBehaviour {

    public int duration, cd;
    public GameObject particle;
    private GameObject obj, Hero;
    public bool onCD;
    public enemyStats me;
    public EnemyAI AI;
    public BlindingLight spell;
    public playerStats player;

	// Use this for initialization
	void Start () {
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        player = Hero.GetComponent<playerStats>();
        me = GetComponent<enemyStats>();
        AI = GetComponent<EnemyAI>();
        spell = Hero.GetComponent<BlindingLight>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!onCD && Vector3.Distance(transform.position, Hero.transform.position) <= 5f && spell.onCD == false && AI.foundPlayer == true && player.dead == false && me.dead == false)
        {
            onCD = true;
            obj = (GameObject)Instantiate(particle, transform.position, Quaternion.identity);
            GameObject.Destroy(obj, duration);
            Invoke("ReEnable", cd + (int)Random.Range(-2f, 2f));
        }
	}

    void ReEnable()
    {
        onCD = false;
    }
}
