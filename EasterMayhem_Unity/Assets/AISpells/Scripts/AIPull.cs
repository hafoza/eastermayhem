﻿using UnityEngine;
using System.Collections;

public class AIPull : MonoBehaviour {

    public int cd;
    public GameObject Hero;
    public GameObject particle;
    private GameObject obj;
    public bool onCD = false;
    public Vector3 newPos;
    public playerStats player;
    public enemyStats me;
    public EnemyAI AI;
    public BlindingLight spell;

	// Use this for initialization
	void Start () {
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        player = Hero.GetComponent<playerStats>();
        me = GetComponent<enemyStats>();
        AI = GetComponent<EnemyAI>();
        spell = Hero.GetComponent<BlindingLight>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(!onCD && Vector3.Distance(transform.position, Hero.transform.position) >= 7f && spell.onCD == false && AI.foundPlayer == true && player.dead == false && me.dead == false)
        {
            onCD = true;
            newPos = transform.position;

            if (Hero.transform.position.x < transform.position.x)
                newPos.x -= 1;
            else
                newPos.x += 1;

            if (Hero.transform.position.z < transform.position.z)
                newPos.z -= 1;
            else
                newPos.z += 1;

            Hero.transform.position = newPos;
            Hero.GetComponent<NavMeshAgent>().destination = newPos;
            obj = (GameObject)Instantiate(particle, Hero.transform.position + Vector3.up * 2, Quaternion.identity);
            Camera.main.GetComponent<mouseInput>().inputWorldPosition = newPos;
            GameObject.Destroy(obj, 0.5f);

            Invoke("ReEnable", cd + (int)Random.Range(-2f, 2f));
        }
	}

    void ReEnable()
    {
        onCD = false;
    }
}
