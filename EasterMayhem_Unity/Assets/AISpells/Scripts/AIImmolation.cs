﻿using UnityEngine;
using System.Collections;

public class AIImmolation : MonoBehaviour {

    public GameObject particle;
    public int cd, duration;
    private GameObject obj, Hero;
    private bool onCD;
    public enemyStats me;
    public EnemyAI AI;
    public BlindingLight spell;
    public playerStats player;

	// Use this for initialization
	void Start () {
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        me = GetComponent<enemyStats>();
        AI = GetComponent<EnemyAI>();
        player = Hero.GetComponent<playerStats>();
        spell = Hero.GetComponent<BlindingLight>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!onCD && Vector3.Distance(transform.position, Hero.transform.position) <= 4f && spell.onCD == false && AI.foundPlayer == true && player.dead == false && me.dead == false)
        {
            onCD = true;
            obj = (GameObject)Instantiate(particle, transform.position, Quaternion.identity);
            obj.transform.parent = transform;
            Invoke("ReEnable", cd + (int)Random.Range(-2f, 2f));
            GameObject.Destroy(obj, duration);
        }
	}

    void ReEnable()
    {
        onCD = false;
    }
}
