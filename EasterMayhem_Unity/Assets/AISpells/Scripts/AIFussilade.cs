﻿using UnityEngine;
using System.Collections;

public class AIFussilade : MonoBehaviour {

    public GameObject projectile;
    [HideInInspector]
    public bool onCD;
    [HideInInspector]
    public GameObject Hero;
    public int cd, numberOfProjectiles;
    private int i;
    public enemyStats me;
    public EnemyAI AI;
    public BlindingLight spell;
    public playerStats player;

	// Use this for initialization
	void Start () {
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        me = GetComponent<enemyStats>();
        AI = GetComponent<EnemyAI>();
        player = Hero.GetComponent<playerStats>();
        spell = Hero.GetComponent<BlindingLight>();
	}
	
	// Update is called once per frame
	void Update () {
        if (!onCD && Vector3.Distance(transform.position, Hero.transform.position) <= 10f && spell.onCD == false && AI.foundPlayer == true && player.dead == false && me.dead == false)
        {
            onCD = true;

            for (i = 1; i <= numberOfProjectiles; i++ )
            {
                Invoke("GenerateBomb", 0.5f * i);
            }
                Invoke("ReEnable", cd + (int)Random.Range(-2f, 2f));
        }
	}

    void GenerateBomb()
    {
        GameObject bomb;
        Vector3 force, target;
        target.x = Random.Range(transform.position.x - 20f, transform.position.x + 20f);
        target.y = 1f;
        target.z = Random.Range(transform.position.z - 20f, transform.position.z + 20f);

        bomb = (GameObject)Instantiate(projectile, transform.position + Vector3.up * 3, Quaternion.identity);

        force.x = target.x - projectile.transform.position.x;
        force.y = Vector3.Distance(projectile.transform.position, target);
        force.z = target.z - projectile.transform.position.z;

        force = force.normalized * Mathf.Sqrt(8f * Vector3.Distance(transform.position, target));

        bomb.GetComponent<Rigidbody>().velocity = force;
    }

    void ReEnable()
    {
        onCD = false;
    }
}
