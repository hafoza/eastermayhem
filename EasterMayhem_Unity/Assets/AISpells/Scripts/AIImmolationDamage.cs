﻿using UnityEngine;
using System.Collections;

public class AIImmolationDamage : MonoBehaviour {

    public int damage;
    [HideInInspector]
    public GameObject Hero;
    private playerStats player;

	// Use this for initialization
	void Start () {
        GetComponent<ParticleSystem>().time = 4;
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        player = Hero.GetComponent<playerStats>();
        InvokeRepeating("Damage", 0, 1);
	}
	
	// Update is called once per frame
	void Damage () {
        if (Vector3.Distance(transform.position, Hero.transform.position) <= 5f)
        {
            player.modifyHP(damage, -1);
        }
	}
}
