﻿using UnityEngine;
using System.Collections;

public class LavaPoolDamage : MonoBehaviour {

    [HideInInspector]
    public GameObject Hero;
    public int damage;

	// Use this for initialization
	void Start () {
        Hero = GameObject.FindGameObjectWithTag("Jesus");
        InvokeRepeating("dmg", 0, 2);
	}
	
	// Update is called once per frame
	void Update () {

	}

    void dmg()
    {
        if (Vector3.Distance(transform.position, Hero.transform.position) <= 15f)
            Hero.GetComponent<playerStats>().modifyHP(damage, -1);
    }

}
