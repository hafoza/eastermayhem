﻿using UnityEngine;
using System.Collections;

public class FussiladeExplode : MonoBehaviour {

    public GameObject particle;
    [HideInInspector]
    public GameObject obj;
    private bool exploded = false;
    public int damage;
    private HolyGrenade spell;
    private GameObject jesus;
    private Quaternion rotation;

	// Use this for initialization
	void Start () {
        rotation = Quaternion.Euler(new Vector3(-90, 0, 0));
        jesus = GameObject.FindGameObjectWithTag("Jesus");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider otherCollider)
    {

        if (otherCollider.tag == "Ground" || otherCollider.GetComponent<playerStats>() && !exploded)
        {
            exploded = true;
            gameObject.GetComponent<MeshRenderer>().enabled = false;
            obj = (GameObject)Instantiate(particle, transform.position, rotation);
            if(gameObject && jesus)
            if (Vector3.Distance(transform.position, jesus.transform.position) <= 8f)
            {
                playerStats pStats = jesus.GetComponent<playerStats>();
                pStats.modifyHP(damage, -1);
            }

            GameObject.Destroy(obj, 1);
            GameObject.Destroy(gameObject);

        }

    }
}
